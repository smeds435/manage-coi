<?php

namespace App\Controllers;

use Symfony\Component\HttpFoundation\Request;
use App\Models\ContactUsForm;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use Silex\Application;

class ContactUsController
{
    private $tokenId = 'contactUs-form';

    public function postForm(Application $app, Request $request)
    {
        $contactForm = new ContactUsForm;

        if(!$this->validateForm($request)){
            return false;
        }

        $contactForm->firstName = $request->get('firstName');
        $contactForm->lastName = $request->get('lastName');
        $contactForm->phone = $request->get('phone');
        $contactForm->email = $request->get('email');
        $contactForm->contactMessage = $request->get('message');

        if($request->get('datetime')){
            $contactForm->preferredDate = new \Carbon\Carbon($request->get('date') . ' ' . $request->get('time'));
        }

        $this->sendMail($app, $request);

        if($contactForm->save()){
            return true;
        }

        return false;
    }

    public function getCsrfToken(Request $request)
    {
        $csrf = new CsrfTokenManager();

        return $csrf->getToken($this->tokenId)->getValue();
    }

    private function validateForm(Request $request)
    {
        $csrf = new CsrfTokenManager();

        if($request->get('form__valid__name') || $request->get('form__valid__email') || $request->get('form__valid__url')){
            return false;
        }

        if(!$csrf->isTokenValid(new CsrfToken($this->tokenId, $request->get('valid')))){
            return false;
        }

        return true;
    }

    private function sendMail(Application $app, Request $request)
    {
        $message = (new \Swift_Message('Get Your Health Record - ' . $request->get('title') . ' Form'))
            ->setFrom('noreply@healthendeavors.com')
            ->setTo('getyourhealthrecord@healthendeavors.com')
            ->setBody('
                <p><b>First Name:</b> ' . $request->get('firstName') . '</p>
                <p><b>Last Name:</b> ' . $request->get('lastName') . '</p>
                <p><b>Phone:</b> ' .$request->get('phone') . '</p>
                <p><b>Email:</b> ' . $request->get('email') . '</p>
                ' . ($request->get('datetime') ? '<p><b>Date: </b> ' . $request->get('date') . '</p>' : '') . '
                ' . ($request->get('datetime') ? '<p><b>Time: </b> ' . $request->get('time') . '</p>' : '') . '
                <b>Message:</b><br>' . $request->get('message') . '</p>
            ')
            ->setContentType('text/html');

        $app['mailer']->send($message);
    }
}