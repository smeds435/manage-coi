<?php

namespace App\Controllers;

use Symfony\Component\HttpFoundation\Request;
use App\Models\EventsCalendar;
use Silex\Application;
use PHPassLib\Hash\BCrypt;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Cookie;

class EventsController
{
    public function indexController()
    {
    	$events = EventsCalendar::whereRaw('CONVERT(date,getdate()) <= case when eventInterval is not null then isnull(endDate, getdate()) else startDate end')->get();

    	$events = $events->sort(function($thisItem,$nextItem){
    		if($thisItem->nextOccurrence() == $nextItem->nextOccurrence()){
				return 0;
    		}
    		return ($thisItem->nextOccurrence() < $nextItem->nextOccurrence()) ? -1 : 1;
    	});

    	return view('events.twig', [
            'events' => $events
        ]);
    }
}
