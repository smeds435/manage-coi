<?php

namespace App;

class Application extends \Silex\Application
{
    protected static $instance = null;

    public function __construct(array $values = array())
    {
        parent::__construct($values);

        static::setInstance($this);
    }

    public static function getInstance()
    {
        return static::$instance ?? new static;
    }

    public static function setInstance(\Silex\Application $container = null)
    {
        static::$instance = $container;
    }

    /**
     * A naive implementation of "resolving" an instance
     * @param  string $concrete
     * @return $concrete
     */
    public function resolve($concrete)
    {
        if(is_string($concrete) && isset(static::$instance[$concrete])) {
            return static::$instance[$concrete];
        }

        $message = "Target [$concrete] not found.";

        throw new \Illuminate\Contracts\Container\BindingResolutionException($message);
    }
}
