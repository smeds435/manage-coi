<?php

use App\Application;

if(! function_exists('app')) {
    /**
     * Get the application instance.
     * @param  string $concrete
     * @return the instance of the $concrete
     */
    function app($concrete = null)
    {
        if(null === $concrete) {
            return Application::getInstance();
        }

        return Application::getInstance()->resolve($concrete);
    }
}

if(! function_exists('twig')) {
    /**
     * Get the twig templateing engine
     */
    function twig()
    {
        return app('twig');
    }
}

if(! function_exists('view')) {
    /**
     * Get the view contents rendered by twig
     * @param  string $view
     * @param  array  $parameters
     * @return string|response?
     */
    function view($view, $parameters = [])
    {
        return twig()->render($view, $parameters);
    }
}
