<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class EventsCalendar extends Eloquent
{
    use SoftDeletes;

	protected $table = 'mhe.eventsCalendar';
    protected $primaryKey = 'EventID';

	public function getDateString()
	{
		if($this->isRecurringEvent()){
			return $this->formatRecurring();
		}

		if($this->endDate){
			return $this->formatEndDay();
		}

		return $this->startDate()->format('F d, Y');
	}

	public function getDay()
	{
		return $this->startDate()->format('d');
	}

	public function getShortMonth()
	{
		return $this->startDate()->format('M');
	}

	public function isRecurringEvent()
	{
		if($this->eventInterval || $this->daysOfWeek || $this->weekInterval || $this->firstDayOfMonth){
			return true;
		}

		return false;
	}

    public function nextOccurrence(){

        if($this->isRecurringEvent()){
            $newDate = Carbon::parse(Carbon::now());
            $returnDate = $this->startDate();

            if($this->firstDayOfMonth){
                $returnDate = $newDate->startOfMonth()->addMonth();
            }else if($this->eventInterval == 'daily'){
                $returnDate = $newDate->addDay();
            }else if($this->eventInterval == 'monthly'){
                $diff = $newDate->diffInMonths($returnDate,false);
                if($diff <= 0){
                   $returnDate->addMonths(abs($diff));
                }
                if($newDate->diffInDays($returnDate,false) <= 0){
                   $returnDate->addMonth();
                }
            }else if($this->eventInterval == 'yearly'){
                $diff = $newDate->diffInYears($returnDate,false);
                if($diff <= 0){
                   $returnDate->addYears(abs($diff));
                }
                if($newDate->diffInDays($returnDate,false) <= 0){
                   $returnDate->addYear();
                }
            }else if($this->daysOfWeek){

                $diff = $newDate->diffInWeeks($returnDate,false);
               if($diff <= 0){
                   $returnDate->addWeeks(abs($diff));
                }
                if($newDate->diffInDays($returnDate,false) <= 0){
                   $returnDate->addWeek();
                }

            }else if($this->weekInterval){

            }

            $this->startDate = $returnDate->format('Y-m-d H:i:s.000');
            return $this->startDate()->format('Y-m-d H:i:s.000');
        }

        return $this->startDate;
    }

	private function startDate()
	{
		return new Carbon($this->startDate);
	}

	private function endDate()
	{
		return new Carbon($this->endDate);
	}

	private function formatEndDay()
	{
		$startDate = $this->startDate()->format('F d');
		$endDate = $this->endDate()->format('F d, Y');
		$showYear = $this->startDate()->format('Y') == $this->endDate()->format('Y') ? false : true;
		$showMonth = $this->startDate()->format('F') == $this->endDate()->format('F') ? false : true;

		if(!$showYear){
			$startDate = $this->startDate()->format('F d');
		}
		if(!$showMonth){
			$endDate = $this->endDate()->format('d, Y');
		}

		if(!$showYear && !$showMonth && $this->startDate()->diffInDays($this->endDate()) == 1){
			return $startDate . ' & ' . $endDate;
		}

		return $startDate . ' - ' . $endDate;
	}

	private function formatRecurring()
	{
        if($this->firstDayOfMonth){
            return 'Every First of the Month';
        }else if($this->eventInterval == 'daily'){
            return 'Every Day';
        }else if($this->eventInterval == 'monthly'){
            return 'Every Month';
        }else if($this->eventInterval == 'yearly'){
            return 'Every Year';
        }else if($this->daysOfWeek){
            return 'Every ' . $this->formatWeedayList($this->daysOfWeek);
        }else if($this->weekInterval){
            return 'Every ' . $this->numberOrdinal($this->weekInterval) . ' Week';
		}

		return null;
	}

    private function dayOfWeektoString($day)
    {
        $dowMap = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

        if(is_numeric($day) && $day > 0 && $day < 8){
            return $dowMap[$day-1];
        }

        throw new \Exception('Day passed into dayOfWeek is out of range.');
    }

    private function formatWeedayList($weekdayList)
    {
        $weekdays = explode(',',$weekdayList);
        $string = '';

        for($i=0;$i<count($weekdays);$i++){
            $day = (int)$weekdays[$i];
            if($i == 0){
                $string = $this->dayOfWeektoString($day);
            }else if($i == count($weekdays)-1){
                $string .= ', and ' . $this->dayOfWeektoString($day);
            }else{
                $string .= ', ' . $this->dayOfWeektoString($day);
            }
        }

        return $string;
    }

    private function numbericOrdinal($number)
    {
        $ends = array('','st','nd','rd','th');
        $pos = 0;

        if(is_numeric($number)){
            $pos = $number;
        }else if(is_numeric($number) && $number < 4){
            $pos = 4;
        }

        return $number . $ends[$pos];
    }
}
