<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ContactUsForm extends Eloquent
{
	protected $table = 'mhe.contactUs';
}
