<?php
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/* @var $app \Silex\Application */

$routes = $app['controllers_factory'];

require __DIR__.'/../routes.php';

$app->mount('/', $routes);