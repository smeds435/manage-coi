<?php

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;
$capsule->addConnection([
	'driver'   => 'sqlsrv',
	'host'     => '192.168.1.84',
	'database' => 'HealthEndeavors',
	'username' => 'sa',
	'password' => 'Ma$t3rk3y',
	'prefix'   => '',
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();