<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation;
use Silex\Application;

$routes->match('/',function(){
    return view('homepage/index.twig');
});

$routes->match('/events','\App\Controllers\EventsController::indexController');
$routes->match('/events/','\App\Controllers\EventsController::indexController');

$routes->match('/register',function(){
    return view('register.twig');
});
$routes->match('/register/',function(){
    return view('register.twig');
});

$routes->post('/contact-form/', '\App\Controllers\ContactUsController::postForm');
$routes->post('/contact-form/token/', '\App\Controllers\ContactUsController::getCsrfToken');
