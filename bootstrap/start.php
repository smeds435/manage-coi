<?php
use Silex\Provider\CsrfServiceProvider;

$app = require __DIR__.'/../app/config/app.php';

$app->register(new CsrfServiceProvider());

$app['debug'] = true;

return $app;